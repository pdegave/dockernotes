Docker Commands :

 - docker run command
 
 Runs the docker container from a docker image on the host if the image is present on the host. If the image is not present on the host, it pulls the image from the DockerHub registry. 
 This is only done once and for the subsequent run commands, the same image is used.
 
 - docker ps 
 
 List the containers running on the system. '-a' lists the containers that are running as well the once exited.
 
 - docker stop <name_container>
 
 Stops the docker container with the given name.
 
 - docker rm <name_container>
 
 Removes the container from the docker host. If you check using 'docker ps -a' command, it will not show up in the exited process.
 
 Tip : The above rm command does not remove the docker image from te host. It only remove the container instance created from that image.
 
 - docker images
 
 Lists the images present on the docker container along with the size each image occupies on the docker host.

 - docker rmi <image>

Removes the docker image from the docker host. Bo dependent container of the image must be running when running the above command.

 - docker pull <image>
 
In this case, the docker just pulls the image from the DockerHub on the docker host and does not run the container. 



Dockers are meant to accomplish certain tasks such as host an application server or a web service or a computation. Docker containers are not meant to host
operating systems, unlike the VM's. A docker container only lives as long as the process inside the docker conatiner is running. Once the process stops, 
the docker container exits.

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
 
 Behind the scene of docker Hello world : 
 
 Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
	
	
	containerd.io - daemon to interface with the OS API (in this case, LXC - Linux Containers), essentially decouples Docker from the OS, also provides container services for non-Docker container managers

docker-ce - Docker daemon, this is the part that does all the management work, requires the other two on Linux

docker-ce-cli - CLI tools to control the daemon, you can install them on their own if you want to control a remote Docker daemon

- All the docker images are stored in below location s: 

[root@localhost sha256]# pwd
/var/lib/docker/image/overlay2/imagedb/content/sha256

- To remove images you need to : 
  remove the container first : docker rm busy_knuth    
  remove the image using cmd : docker rmi busy_knuth
  
  
  F) Connect to mysql server container : 
  
  Ref : https://stackoverflow.com/questions/28389458/execute-mysql-command-from-the-host-to-container-running-mysql-server
  
  
 Below are multiple ways: 
 
 A) [root@localhost yum.repos.d]# docker exec -it test-mysql bash -l             
  root@cc73e94f3516:/# mysql -u root -p                                        
  Enter password:                                                              
  Welcome to the MySQL monitor.  Commands end with ; or \g.                    
  Your MySQL connection id is 13                                               
  Server version: 8.0.19 MySQL Community Server - GPL                          
                                                                               
  Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved. 
                                                                               
  Oracle is a registered trademark of Oracle Corporation and/or its            
  
  B) [root@localhost yum.repos.d]# docker exec -t -i test-mysql /bin/bash
root@cc73e94f3516:/# mysql -u root -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 12
Server version: 8.0.19 MySQL Community Server - GPL

  C) docker exec -i some_mysql_container test-mysql mysql -u root -p mypassword  <<< "select database();"



 G) Mysql on Docker : 
 
 https://severalnines.com/database-blog/mysql-docker-containers-understanding-basics
 
 

